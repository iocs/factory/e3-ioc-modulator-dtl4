require essioc
require modbus
require modulator

epicsEnvSet(SYS, "DTL")
epicsEnvSet(SUB, "040")
epicsEnvSet(ID, "040")
epicsEnvSet(MOD_IP, "dtl-040-modulator.tn.esss.lu.se")
epicsEnvSet(PSS_PV, "FEB-010Row:CnPw-U-004:ToDTL4LPS")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

dbLoadRecords("$(E3_CMD_TOP)/pss_bypass.db","P=$(SYS)-$(SUB):RFS-Mod-$(ID):")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

